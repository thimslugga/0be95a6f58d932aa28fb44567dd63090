#!/bin/bash

# Get region
AZ=$(bash -c "curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone")
REGION=${AZ::-1}

IFS=', ' read -r -a STATS <<< `nvidia-smi --query-gpu=temperature.gpu,memory.used,memory.free,utilization.gpu,utilization.memory --format=csv,nounits,noheader`
#echo ${STATS[@]}

# Send values to CloudWatch
aws cloudwatch --region $REGION put-metric-data --metric-name Temperature --namespace GPUStats --value ${STATS[0]}
aws cloudwatch --region $REGION put-metric-data --metric-name MemoryUsed --namespace GPUStats --unit Megabytes --value ${STATS[1]}
aws cloudwatch --region $REGION put-metric-data --metric-name MemoryFree --namespace GPUStats --unit Megabytes --value ${STATS[2]}
aws cloudwatch --region $REGION put-metric-data --metric-name GPUUtilization --namespace GPUStats --value ${STATS[3]}
aws cloudwatch --region $REGION put-metric-data --metric-name MemoryUtilization --namespace GPUStats --value ${STATS[4]}